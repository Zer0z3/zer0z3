import Foundation

//Para escolher quando o valor do speedatk e o speeddef é igual
var luckySpeed:Int?

//Para identificar quando o atk resolve
var atkSuccess:Bool = false

//Valor random para ver se o ataque é feito ou dá miss
var accuracyRoll:Int = 0
var tempDEF:Int = 0
var tempATK:Int = 0

//Nomes dos Pokemon's
var pokemonP1:String = ""
var pokemonP2:String = ""

//Identificação do PP em falta
var attackPP1:[Int] = [0, 0, 0, 0]
var attackPP2:[Int] = [0, 0, 0, 0]

//Class de status do pokemon
class pokemon {
    var HP1:Int = 0
    var ATK1:Int = 0
    var DEF1:Int = 0
    var SPATK1:Int = 0
    var SPDEF1:Int = 0
    var Speed1:Int = 0
    
    var HP2:Int = 0
    var ATK2:Int = 0
    var DEF2:Int = 0
    var SPATK2:Int = 0
    var SPDEF2:Int = 0
    var Speed2:Int = 0
}

//Class de info de ataque dos pokemon's
class attack  {
    var PP:Int = 0
    var Power:Int = 0
    var Accuracy:Int = 0
    var atkName:String = ""
}

//Inicio
print("Choose your Pokemon:")
print("")
print("1. Batman")
print("2. Kratos")
print("3. BigDaddy")
print("4. Solid_Snake")
print("5. Heihachi")
print("6. Raiden")

var pokemon1 = readLine()

var pokemon2:Int = 0

let pokemonClass = pokemon()
let attackClass = attack()

//Random Player 2
repeat {
    
    pokemon2 = Int(arc4random_uniform(7))
    
    if pokemon2==0 {
        pokemon2 += 1
    }
    
} while pokemon2 == Int(pokemon1!)

//Assign Pokemon to player
func pokemonAssign(pokemon:Int) {
    switch (pokemon) {
    case 1:
        if (pokemonClass.HP1 == 0) {
        pokemonP1 = "Batman"
        pokemonClass.HP1 = 200
        pokemonClass.ATK1 = 120
        pokemonClass.DEF1 = 80
        pokemonClass.SPATK1 = 150
        pokemonClass.SPDEF1 = 150
        pokemonClass.Speed1 = 180
        }
        else {
        pokemonP2 = "Batman"
        pokemonClass.HP2 = 200
        pokemonClass.ATK2 = 120
        pokemonClass.DEF2 = 80
        pokemonClass.SPATK2 = 150
        pokemonClass.SPDEF2 = 150
        pokemonClass.Speed2 = 180
        }
        break
    case 2:
        if (pokemonClass.HP1 == 0) {
        pokemonP1 = "Kratos"
        pokemonClass.HP1 = 250
        pokemonClass.ATK1 = 120
        pokemonClass.DEF1 = 100
        pokemonClass.SPATK1 = 100
        pokemonClass.SPDEF1 = 100
        pokemonClass.Speed1 = 100
        }
        else {
        pokemonP2 = "Kratos"
        pokemonClass.HP2 = 250
        pokemonClass.ATK2 = 120
        pokemonClass.DEF2 = 100
        pokemonClass.SPATK2 = 100
        pokemonClass.SPDEF2 = 100
        pokemonClass.Speed2 = 100
        }
        break
    case 3:
        if (pokemonClass.HP1 == 0) {
        pokemonP1 = "BigDaddy"
        pokemonClass.HP1 = 300
        pokemonClass.ATK1 = 150
        pokemonClass.DEF1 = 150
        pokemonClass.SPATK1 = 50
        pokemonClass.SPDEF1 = 50
        pokemonClass.Speed1 = 50
        }
        else {
        pokemonP2 = "BigDaddy"
        pokemonClass.HP2 = 300
        pokemonClass.ATK2 = 150
        pokemonClass.DEF2 = 150
        pokemonClass.SPATK2 = 50
        pokemonClass.SPDEF2 = 50
        pokemonClass.Speed2 = 50
        }
        break
    case 4:
        if (pokemonClass.HP1 == 0) {
        pokemonP1 = "Solid_Snake"
        pokemonClass.HP1 = 170
        pokemonClass.ATK1 = 120
        pokemonClass.DEF1 = 50
        pokemonClass.SPATK1 = 140
        pokemonClass.SPDEF1 = 140
        pokemonClass.Speed1 = 120
        }
        else {
        pokemonP2 = "Solid_Snake"
        pokemonClass.HP2 = 170
        pokemonClass.ATK2 = 120
        pokemonClass.DEF2 = 50
        pokemonClass.SPATK2 = 140
        pokemonClass.SPDEF2 = 140
        pokemonClass.Speed2 = 120
        }
        break
    case 5:
        if (pokemonClass.HP1 == 0) {
        pokemonP1 = "Heihachi"
        pokemonClass.HP1 = 250
        pokemonClass.ATK1 = 150
        pokemonClass.DEF1 = 50
        pokemonClass.SPATK1 = 120
        pokemonClass.SPDEF1 = 120
        pokemonClass.Speed1 = 100
        }
        else {
        pokemonP2 = "Heihachi"
        pokemonClass.HP2 = 250
        pokemonClass.ATK2 = 150
        pokemonClass.DEF2 = 50
        pokemonClass.SPATK2 = 120
        pokemonClass.SPDEF2 = 120
        pokemonClass.Speed2 = 100
        }
        break
    case 6:
        if (pokemonClass.HP1 == 0) {
        pokemonP1 = "Raiden"
        pokemonClass.HP1 = 200
        pokemonClass.ATK1 = 150
        pokemonClass.DEF1 = 100
        pokemonClass.SPATK1 = 120
        pokemonClass.SPDEF1 = 80
        pokemonClass.Speed1 = 100
        }
        else {
        pokemonP2 = "Raiden"
        pokemonClass.HP2 = 200
        pokemonClass.ATK2 = 150
        pokemonClass.DEF2 = 100
        pokemonClass.SPATK2 = 120
        pokemonClass.SPDEF2 = 80
        pokemonClass.Speed2 = 100
        }
        break
    default:
        break
    }
}

pokemonAssign(pokemon: Int(pokemon1!)!)
pokemonAssign(pokemon: pokemon2)

print("********************************************")
print("*  " + pokemonP1 + " VS " + pokemonP2 + "  *")
print("********************************************")

var attackChosen:Int = 0

//Função para inserir os valores segundo o ataque escolhido nas devidas variáveis
func chosenAttack(chosenPokemon:Int, attackChosen:Int) {
    
    switch chosenPokemon {
    case 1:
        switch attackChosen {
        case 1:
            attackClass.atkName = "Batarang"
            attackClass.PP = 10
            attackClass.Power = 2
            attackClass.Accuracy = 10
            break
        case 2:
            attackClass.atkName = "Sky Grapple"
            attackClass.PP = 7
            attackClass.Power = 4
            attackClass.Accuracy = 8
            break
        case 3:
            attackClass.atkName = "Cape Counter"
            attackClass.PP = 7
            attackClass.Power = 0
            attackClass.Accuracy = 8
            break
        case 4:
            attackClass.atkName = "Scatter Bombs"
            attackClass.PP = 5
            attackClass.Power = 0
            attackClass.Accuracy = 10
            break
        default:
            break
        }
        break
    case 2:
        switch attackChosen {
        case 1:
            attackClass.atkName = "Axe Chop"
            attackClass.PP = 10
            attackClass.Power = 5
            attackClass.Accuracy = 8
            break
        case 2:
            attackClass.atkName = "Blade Throw"
            attackClass.PP = 7
            attackClass.Power = 8
            attackClass.Accuracy = 7
            break
        case 3:
            attackClass.atkName = "Barrel Roll"
            attackClass.PP = 8
            attackClass.Power = 0
            attackClass.Accuracy = 10
            break
        case 4:
            attackClass.atkName = "Shield"
            attackClass.PP = 5
            attackClass.Power = 0
            attackClass.Accuracy = 7
            break
        default:
            break
        }
        break
    case 3:
        switch attackChosen {
        case 1:
            attackClass.atkName = "Perfuration"
            attackClass.PP = 10
            attackClass.Power = 7
            attackClass.Accuracy = 8
            break
        case 2:
            attackClass.atkName = "Electro Bolt"
            attackClass.PP = 7
            attackClass.Power = 8
            attackClass.Accuracy = 5
            break
        case 3:
            attackClass.atkName = "Winter Blast"
            attackClass.PP = 7
            attackClass.Power = 2
            attackClass.Accuracy = 5
            break
        case 4:
            attackClass.atkName = "Decoy"
            attackClass.PP = 4
            attackClass.Power = 0
            attackClass.Accuracy = 4
            break
        default:
            break
        }
        break
    case 4:
        switch attackChosen {
        case 1:
            attackClass.atkName = "Shot"
            attackClass.PP = 10
            attackClass.Power = 5
            attackClass.Accuracy = 7
            break
        case 2:
            attackClass.atkName = "Stealth Takedown"
            attackClass.PP = 4
            attackClass.Power = 8
            attackClass.Accuracy = 7
            break
        case 3:
            attackClass.atkName = "Hide"
            attackClass.PP = 5
            attackClass.Power = 0
            attackClass.Accuracy = 5
            break
        case 4:
            attackClass.atkName = "Snake in a box"
            attackClass.PP = 2
            attackClass.Power = 7
            attackClass.Accuracy = 4
            break
        default:
            break
        }
        break
    case 5:
        switch attackChosen {
        case 1:
            attackClass.atkName = "Demon Slayer"
            attackClass.PP = 8
            attackClass.Power = 5
            attackClass.Accuracy = 8
            break
        case 2:
            attackClass.atkName = "Demon Breath"
            attackClass.PP = 4
            attackClass.Power = 7
            attackClass.Accuracy = 7
            break
        case 3:
            attackClass.atkName = "Defend"
            attackClass.PP = 5
            attackClass.Power = 0
            attackClass.Accuracy = 8
            break
        case 4:
            attackClass.atkName = "Immobilize"
            attackClass.PP = 4
            attackClass.Power = 0
            attackClass.Accuracy = 4
            break
        default:
            break
        }
        break
    case 6:
        switch attackChosen {
        case 1:
            attackClass.atkName = "Lightning"
            attackClass.PP = 7
            attackClass.Power = 7
            attackClass.Accuracy = 5
            break
        case 2:
            attackClass.atkName = "Vicinity Blast"
            attackClass.PP = 5
            attackClass.Power = 7
            attackClass.Accuracy = 8
            break
        case 3:
            attackClass.atkName = "Teleport"
            attackClass.PP = 4
            attackClass.Power = 0
            attackClass.Accuracy = 5
            break
        case 4:
            attackClass.atkName = "Static Trap"
            attackClass.PP = 5
            attackClass.Power = 0
            attackClass.Accuracy = 4
            break
        default:
            break
        }
        break
    default:
        break
    }
    
}

//Função de resolução do ataque
func attackResult(plyrTurn:Bool, attackChosen:Int) {
    accuracyRoll = Int(arc4random_uniform(11))
    if (plyrTurn==false){
        
        //Player 1
        
        attackClass.PP -= attackPP1 [attackChosen]
        if (pokemonClass.SPATK1 == pokemonClass.SPDEF2) {
            luckySpeed = Int(arc4random_uniform(11))
        }
      if (attackClass.PP > 0) {
        attackPP1.insert(+1, at: attackChosen)
        if (attackClass.Accuracy >= accuracyRoll) {
            if let x = luckySpeed {
            if (pokemonClass.SPATK1 > pokemonClass.SPDEF2 || luckySpeed! > 5) {
                tempDEF = Int(arc4random_uniform(4))
                tempDEF = pokemonClass.DEF2 - tempDEF
            }
            else {
                tempDEF = Int(arc4random_uniform(4))
                tempDEF = pokemonClass.DEF2 + tempDEF
            }
            }
            tempATK = pokemonClass.ATK1 - tempDEF
            pokemonClass.HP2 -= tempATK
            atkSuccess = true
        }
        else {
            print("")
            print("Attack Failed!!")
        }
      }
        else {
        print("")
        print("Unsufficient PP")
        atkSuccess = false
        }
        
    }
    else {
        
        //Player 2
        
        attackClass.PP -= attackPP2 [attackChosen]
        if (pokemonClass.SPATK2 == pokemonClass.SPDEF1) {
            luckySpeed = Int(arc4random_uniform(11))
        }
        if (attackClass.PP > 0) {
            attackPP2.insert(+1, at: attackChosen)
            if (attackClass.Accuracy >= accuracyRoll) {
                if let x = luckySpeed {
                if (pokemonClass.SPATK2 > pokemonClass.SPDEF1 || luckySpeed! > 5) {
                    tempDEF = Int(arc4random_uniform(4))
                    tempDEF = pokemonClass.DEF1 - tempDEF
                }
                else {
                    tempDEF = Int(arc4random_uniform(4))
                    tempDEF = pokemonClass.DEF1 + tempDEF
                }
                }
                tempATK = pokemonClass.ATK2 - tempDEF
                pokemonClass.HP1 -= tempATK
                atkSuccess = true
            }
            else {
                print("")
                print("Attack Failed!!")
            }
        }
        else {
            print("")
            print("Unsufficient PP")
            atkSuccess = false
        }
        
    }
    
}

var playerTurn:Bool = false
var repeat2:Bool = false

repeat {
    
    if (repeat2 == false) {
        
    if (pokemonClass.Speed1 == pokemonClass.Speed2) {
        luckySpeed = Int(arc4random_uniform(11))
    }
        if let x = luckySpeed {
    if (pokemonClass.Speed1 > pokemonClass.Speed2 || luckySpeed! > 5 ) {
        luckySpeed = nil
        playerTurn = false
    }
    else if (pokemonClass.Speed2 > pokemonClass.Speed1 || luckySpeed! < 5){
        luckySpeed = nil
        playerTurn = true
    }
        }
    }
    
    //Player1
    
    if (playerTurn == false) {
        print("")
        repeat {
    switch Int(pokemon1!) {
    case 1:
        print("1. Batarang\n2. Sky Grapple\n3. Cape Counter\n4. Scatter Bombs")
        break
    case 2:
        print("1. Axe Chop\n2. Blade Throw\n3. Barrel Roll\n4. Shield")
        break
    case 3:
        print("1. Perfuration\n2. Electro Bolt\n3. Winter Blast\n4. Decoy")
        break
    case 4:
        print("1. Shot\n2. Stealth Takedown\n3. Hide\n4. Snake in a box")
        break
    case 5:
        print("1. Demon Slayer\n2. Demon Breath\n3. Defend\n4. Immobilize")
        break
    case 6:
        print("1. Lightning\n2. Vicinity Blast\n3. Teleport\n4. Static Trap")
        break
    default:
        break
    }
    
    attackChosen = Int(readLine()!)!
    chosenAttack(chosenPokemon: Int(pokemon1!)!, attackChosen: attackChosen)
            
            print("")
            print("Player 1 attacks with " + attackClass.atkName)
            
    attackResult(plyrTurn:playerTurn, attackChosen: attackChosen)
        
        } while atkSuccess == false
    
    if (pokemonClass.HP2 <= 0){
        break
    }
        print("")
    print("P1 Life: ", pokemonClass.HP1)
    print("P2 Life: ", pokemonClass.HP2)
    playerTurn = true
        
    }
    
    //
    
    //Player 2
    
    if (playerTurn == true) {
        attackChosen = Int(arc4random_uniform(5))
        if (attackChosen == 0) {
            attackChosen+=1
        }
        chosenAttack(chosenPokemon: Int(pokemon2), attackChosen: attackChosen)
        
        print("")
        print("Player 2 attacks with " + attackClass.atkName)
        
        attackResult(plyrTurn:playerTurn, attackChosen: attackChosen)
        
        if (pokemonClass.HP1 <= 0){
            break
        }
        print("")
        print("P1 Life: ", pokemonClass.HP1)
        print("P2 Life: ", pokemonClass.HP2)
        playerTurn = false
    }
    
    //
    
    if (repeat2 == true) {
        repeat2 = false
    }
    else {
        repeat2 = true
    }
    
} while pokemonClass.HP1 <= 0 || pokemonClass.HP2 <= 0

if (pokemonClass.HP1 <= 0) {
    print("")
    print ("****************")
    print ("* PLAYER 1 WON *")
    print ("****************")
}
else if (pokemonClass.HP2 <= 0) {
    print("")
    print ("****************")
    print ("* PLAYER 2 WON *")
    print ("****************")
}
else if (pokemonClass.HP1 > 0 && pokemonClass.HP2 > 0) {
    print("ERROR!")
}

